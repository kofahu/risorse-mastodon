# Risorse Mastodon [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome) [![License: CC0](https://img.shields.io/badge/License-CC0-lightgrey.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

[<img src="https://rawgit.com/tleb/awesome-mastodon/master/mastodon-logo.svg" align="right" width="150">](https://joinmastodon.org)

> Curated list of Mastodon-related stuff!

**Mastodon is the world's largest free, open-source, decentralized microblogging network**. See the [Official](#official) category to learn more about the network.

## Contents

* [Official](#official)
* [Clients](#clients)
* [Federated servers](#federated-servers)
* [Tools](#tools)
* [User styles](#user-styles)
* [User scripts](#user-scripts)
* [Bots](#bots)
* [News & magazines bots](#news--magazines-bots)
* [Associations](#associatons)
* [Blogs](#blogs)
* [Libraries](#libraries)
* [Hosting](#hosting)

## Official

* [Website](https://joinmastodon.org)
* [GitHub repository](https://github.com/tootsuite/mastodon)
* [Documentation & official explaination](https://docs.joinmastodon.org/)
* [API documentation](https://docs.joinmastodon.org/client/intro/)

## Clients

* [Tusky](https://play.google.com/store/apps/details?id=com.keylesspalace.tusky) - Android client.
* [Twidere](https://f-droid.org/packages/org.mariotaku.twidere/) - Android app for Twitter, GNU Social and Mastodon.
* [Tooty](https://github.com/n1k0/tooty) - Experimental multi-account Mastodon Web client (Elm).
* [toot](https://github.com/ihabunek/toot) - Mastodon CLI client (Python).
* [madonctl](https://github.com/McKael/madonctl) - Mastodon CLI client (Go).
* [naumanni](https://github.com/naumanni/naumanni) - Web user interface specially designed for Mastodon.
* [Tooter](https://github.com/dysk0/harbour-tooter) - Native client for SailfishOS.
* [Fedilab](https://framagit.org/tom79/fedilab) - Android client.
* [Pinafore](https://github.com/nolanlawson/pinafore) - Alternative web client for Mastodon, focused on speed and simplicity.
* [Tootle](https://github.com/bleakgrey/tootle) - Simple Mastodon client designed for elementary OS.
* [Brutaldon](https://git.carcosa.net/jmcbray/brutaldon) - Brutaldon is a brutalist, Web 1.0 web interface for Mastodon.
* [Halcyon](https://notabug.org/halcyon-suite/halcyon) - Alternative web client for Mastodon and Pleroma with a Twitter-like interface.
* [Whalebird](https://whalebird.social/en/desktop/contents) - Electron-based Mastodon client.
* [Planiverse](https://git.mulligrubs.me/planiverse/) - Minimalist, no-JS Web client for Mastodon.
* [Toot!](https://apps.apple.com/us/app/toot/id1229021451) - Mastodon client for iOS.

## Federated servers

* [Mastodon](https://joinmastodon.org/) - Most known microblogging platform.
* [Pleroma](https://pleroma.social/) - Lightweight microblogging platform.
* [GnuSocial](https://gnu.io/social/) - Oldest microblogging platform.
* [Microblog.pub](https://microblog.pub/) - Single-user lightweight microblogging platform.
* [Hubzilla](https://zotlabs.org/page/hubzilla/hubzilla-project) - Blog/social networks platform with file, contacts and events sharing.
* [Friendica](https://friendi.ca/) - Social network platform.
* [Peertube](https://joinpeertube.org/) - Video sharing platform.
* [FunkWhale](https://funkwhale.audio/) - Audio sharing platform.
* [Plume](https://joinplu.me/) - Blogging platform.
* [WriteFreely](https://writefreely.org/) - Blogging platform.
* [Prismo](https://gitlab.com/prismosuite/prismo) - Link aggregation platform.
* [PixelFed](https://pixelfed.org/) - Photograph sharing platform.
* [NextCloud Social](https://apps.nextcloud.com/apps/social) - Microblogging inside the cloud platform.

## Tools

* [Mastodon instances](https://instances.social/list) - List of Mastodon instances.
* [unmung.com/mastoview](http://www.unmung.com/mastoview) - Preview the local or federated timeline of any instance.
* [Toot scheduler](https://scheduler.mastodon.tools/) - Schedule now, toot later.
* [Last](https://framagit.org/luc/last) - Aggregate toots on a web page providing Atom feed and an epub (Perl).
* [Forget](https://forget.codl.fr/about/) - Delete toots after a user defined period of time (Python [source code](https://github.com/codl/forget/)).
* [Mastodon Toot Bookmarklet](https://rknightuk.github.io/mastodon-toot-bookmarklet/) - Bookmarklet to toot the current page ([source code](https://github.com/rknightuk/mastodon-toot-bookmarklet/))
* [Mastodon – Simplified Federation!](https://addons.mozilla.org/firefox/addon/mastodon-simplified-federation/) - Redirect clicks on remote follow/interaction buttons to your own instance ([source code](https://github.com/rugk/mastodon-simplified-federation)).
* [Fediverse Explorer](https://fediverse.0qz.fun/) - Trending hashtags and popular toots, regenerated every hour.


## User styles

* [Variable width](https://userstyles.org/styles/139721/mastodon-glitch-soc-variable-width) - Makes Mastodon scale with the browser's width.
* [Narrow drawer](https://userstyles.org/styles/141457/mastodon-dynamic-wide-columns-narrow-drawer) - With the variable width style, makes the drawer narrower and the other columns scale accordingly.

## User scripts

* [NSFW Remover](https://greasyfork.org/fr/scripts/29228-mastodon-nsfw-remover) - Automatically display NSFW images.
* [Customizable interface](https://openuserjs.org/scripts/bl00m/Mastodon_Customizable_Interface) - Move and resize columns on a grid.
* [BirdSite](https://gitlab.com/pmorinerie/birdsite) - Browser extension for cross-posting Mastodon toots to Twitter.

## Bots

* [feed2toot](https://gitlab.com/chaica/feed2toot) - Automatically parses RSS feeds, identifies new posts and posts them on Mastodon (Python).
* [usercount](https://github.com/josefkenny/usercount) - Bot which posts user statistics to Mastodon (Python).
* [autofollow](https://github.com/gled-rs/mastodon-autofollow) - Autofollow bot for Mastodon (Python).
* [hnbot](https://github.com/raymestalez/mastodon-hnbot) - Posts the Hacker News stories with 100+ points (Python).
* [translator](https://christopher.su/projects/translator/) - Translate any toot into the desired language using `@translator@toot.works [langcode]`.
* [@TrendingBot@mastodon.social](https://mastodon.social/@TrendingBot) - Shows you what's trending on Mastodon.
* [Remindr](https://gitlab.com/chaica/remindr) - Automatically send reminders to both Mastodon and Twitter from a list of resources (Python).
* [News Bot](https://botsin.space/@newsbot) - mirrors Twitter accounts on Mastodon (ClojureScript), source available on [GitHub](https://github.com/yogthos/mastodon-bot).
* [Welcome Bot](https://github.com/indyhall/mastodon-welcome-bot) - Automatically send a welcome DM to new users (Python).

## News & magazines bots

Please note: most of them are non-official. Also, we do not *recommend* any, they are just listed here for your information.

### English

* [@fsf@status.fsf.org](https://status.fsf.org/fsf) - FSF Free Software Foundation.
* [@HackerNewsBot@mastodon.social](https://mastodon.social/@HackerNewsBot) - Hacker News.
* [@btc@framapiaf.org](https://framapiaf.org/@btc) - News about the Bitcoin Cryptocurrency (marketcap, community, rise/drop alerts).
* [@ethereum@framapiaf.org](https://framapiaf.org/@ethereum) - News about the Ethereum Cryptocurrency (marketcap, community, rise/drop alerts).
* [@monero@framapiaf.org](https://framapiaf.org/@monero) - News about the Monero Cryptocurrency (marketcap, community, rise/drop alerts).
* [@launchradar@mastodon.cloud](https://mastodon.cloud/@launchradar) - News about space flight, astronomy and astrophysics.

### Italiano

* [@diggita@mastodon.uno](https://mastodon.uno/@diggita) - diggita.
* [@gayburg@mastodon.uno](https://mastodon.uno/@gayburg) - Gayburg.

## Software libero 

### English
* [@fsfe@mastodon.social](https://mastodon.social/@fsfe)
* [@fsf@hostux.soxial](https://hostux.social/@fsf)

### Italiano

* [@lealternative@mastodon.uno](https://mastodon.uno/@lealternative) - Le Alternative.
* [libreadvice@mastodon.uno](https://mastodon.uno/libreadvice) - Libre Advice

## Associations

### English

* [@torproject@mastodon.social](https://mastodon.social/@torproject) - Tor Project.

### Italiano

* [@devol@mastodon.uno](https://mastodon.uno/@devol) - colletivo devol.


## Blogs

### English

### Italiano



## Libraries

* [Mastodon.py](https://github.com/halcy/Mastodon.py) - Python wrapper for the Mastodon API.
* [Megalodon](https://github.com/h3poteto/megalodon) - Mastodon API client library for node.js.

